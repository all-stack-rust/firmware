#![no_main]
#![no_std]

pub mod page_store;
use crate::page_store::StaticStorage;

use stm32f407g_disc::{
    nb::block,
    hal::{
        serial::{
            Event,
            Rx,
            Tx,
            config::Config,
            Serial
        },
        timer::{
            Timer,
            Event::TimeOut
        },
        prelude::*
    },
    stm32 as stm32f407,
    ADC1,
    adc::{
        config::{
            AdcConfig,
            SampleTime,
            Eoc,
            Sequence
        },
        Adc
    }
};
use heapless::{Vec, consts::*};

use rtt_target::{rtt_init_print, rprintln};
use core::panic::PanicInfo;

use server::{Server, WriteTcpResult, ResponseType};
use communication::Tx as Message;

type InputBuffer = Vec<u8, U600>;

#[rtic::app(device = stm32f407)]
const APP: () = {
    struct Resources {
        // A resource
        rx: Rx<stm32f407::USART2>,
        tx: Tx<stm32f407::USART2>,
        adc1: Adc<ADC1>,
        timer: Timer<stm32f407::TIM2>
    }

    #[init(spawn = [server_task])]
    fn init(cx: init::Context) -> init::LateResources {
        // Setup RTTI
        rtt_init_print!();

        // Setup peripherals
        let peripherals = stm32f407::Peripherals::take().unwrap();
        let gpioa = peripherals.GPIOA.split();
        let rcc = peripherals.RCC.constrain();
        let clocks = rcc.cfgr.sysclk(168.mhz()).freeze();

        // Set up USART 2 configured pins and a baudrate of 115200 baud
        let tx = gpioa.pa2.into_alternate_af7();
        let rx = gpioa.pa3.into_alternate_af7();
        let mut serial = Serial::usart2(
            peripherals.USART2,
            (tx, rx),
            Config::default().baudrate(115200.bps()),
            clocks,
        ).unwrap();
        serial.listen(Event::Rxne);
        // Separate out the sender and receiver of the serial port
        let (tx, rx) = serial.split();


        // Setup adc on pin A4
        let config = AdcConfig::default().end_of_conversion_interrupt(Eoc::Conversion);
        let mut adc1 = Adc::adc1(peripherals.ADC1, true, config);
        let pa4 = gpioa.pa4.into_analog();
        adc1.configure_channel(&pa4, Sequence::One, SampleTime::Cycles_112);


        //Configure the timer with 1kHz
        let mut timer = Timer::tim2(peripherals.TIM2, 1000.hz(), clocks);
        timer.listen(TimeOut);


        // Spawn the server task once to allow it to initialize the server
        cx.spawn.server_task(None, None).unwrap();

        init::LateResources { rx, tx, adc1, timer}
    }

    #[idle]
    fn idle(_: idle::Context) -> ! {
        loop {
            cortex_m::asm::nop();
        }
    }

    /**
    * Usart rx task. Received bytes from the browser, waits for the request to be complete and then
    * forwards it to the server.
    * Since we directly forward the requests to the device we can not add any sort of framing easily.
    * But we can check for /r/n/r/n which is sent at the end of a HTTP request (without payload).
    * This is not ideal since it has various drawbacks, but works for a proof of concept.
    */
    #[task(binds = USART2, resources = [rx], spawn = [server_task], priority = 3)]
    fn rx_usart2(cx: rx_usart2::Context) {
        static mut INPUT_BUFFER: Option<InputBuffer> = None;
        let rx: &mut Rx<stm32f407::USART2> = cx.resources.rx;

        if INPUT_BUFFER.is_none() {
            *INPUT_BUFFER = Some(InputBuffer::new());
        }

        match block!(rx.read()) {
            Ok(byte) => {
                match INPUT_BUFFER.as_mut().unwrap().push(byte) {
                    Ok(_) => {}
                    _ => {
                        *INPUT_BUFFER = None;
                        return;
                    }
                }

                // Once the frame is received, forward it to the server task
                if frame_received(byte,INPUT_BUFFER.as_ref().unwrap()) {
                    rprintln!("Received request");
                    let send = INPUT_BUFFER.take();
                    cx.spawn.server_task(send, None).ok();
                }
            }
            _ => {
                rprintln!("Receive error");
            }
        }
    }

    /**
    * Timer task that will start the adc 1 conversion
    */
    #[task(binds = TIM2, resources = [timer, adc1])]
    fn tim2(cx: tim2::Context) {
        let timer: &mut Timer<stm32f407::TIM2> = cx.resources.timer;
        let adc1: &mut Adc<ADC1> = cx.resources.adc1;
        adc1.start_conversion();
        timer.clear_interrupt(TimeOut);
    }

    /**
    * Adc task that will collect samples until the message is complete and will the forward it to
    * the server.
    */
    #[task(binds = ADC, resources = [adc1], spawn = [server_task])]
    fn adc_task(cx: adc_task::Context) {
        static mut MESSAGE: Option<Message> = None;
        let adc1: &mut Adc<ADC1> = cx.resources.adc1;

        if MESSAGE.is_none() {
            *MESSAGE = Some(
                Message {
                    sample_time: 0.001f32,
                    samples: Vec::new()
                }
            )
        }

        let milli_volts = adc1.current_sample();
        adc1.clear_end_of_conversion_flag();

        MESSAGE.as_mut().unwrap().samples.push(milli_volts as f32).unwrap();

        let samples_pushed = MESSAGE.as_ref().unwrap().samples.len();
        let samples_capacity = MESSAGE.as_ref().unwrap().samples.capacity();

        if samples_pushed == samples_capacity {
            let message = MESSAGE.take();
            cx.spawn.server_task(None, message).ok();
        }
    }

    /**
    * The server task will either be started by the adc task if a message is complete, or a
    * successful frame receive.
    * It will forward data to the server and respond to the UART accordingly.
    */
    #[task(resources = [tx], capacity = 4, priority = 2)]
    fn server_task(cx: server_task::Context, tcp: Option<InputBuffer>, message: Option<Message>) {
        static mut SERVER: Option<Server<StaticStorage, 4000>> = None;

        if SERVER.is_none() {
            rprintln!("Server booted");
            *SERVER = Some(Server::new(StaticStorage::new()))
        }

        let tx: &mut Tx<stm32f407::USART2> = cx.resources.tx;
        if let Some(tcp_data) = tcp {
            if let Some(server) = SERVER {
                let resp = server.put_tcp(&tcp_data);
                send_tcp_response(resp, tx);
            }
        }
        if let Some(m) = message {
            if let Some(server) = SERVER {
                let ser: Vec<u8, U1020> = Message::serialize(&m).unwrap();
                let resp = server.put_data(&ser[..]);
                match resp {
                    Ok(data) => {
                        data.into_iter().for_each(|b| {
                            block!(tx.write(*b)).unwrap();
                        })
                    }
                    _ => {}
                }
            }
        }
    }

    extern "C" {
        fn TIM3();
    }
};


#[inline(never)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    rprintln!("Panic: {}", info);
    loop {} // You might need a compiler fence in here.
}

/**
* Detect if a full HTTP request was received
*/
fn frame_received(byte: u8, frame: &[u8]) -> bool {

    if byte != "\n".as_bytes()[0] {
        return false;
    }

    let frame_end: Vec<u8, U4> = frame.iter().rev().take(4).rev().map(|b| *b).collect();

    if frame_end.len() != 4 {
        return false;
    }

    if frame_end[0] != "\r".as_bytes()[0] {
        return false;
    }

    if frame_end[1] != "\n".as_bytes()[0] {
        return false;
    }

    if frame_end[2] != "\r".as_bytes()[0] {
        return false;
    }

    if frame_end[3] != "\n".as_bytes()[0] {
        return false;
    }

    return true;
}

fn send_tcp_response(tcp: WriteTcpResult, tx: &mut Tx<stm32f407::USART2>) {
    match tcp {
        Ok(response) => {
            match response {
                ResponseType::TCP(iter) => {
                    iter.for_each(|b| {
                        block!(tx.write(*b)).unwrap();
                    });
                    rprintln!("Send response");
                }
                // Technically we could also receive a websocket package, but we are not
                // supporting that for the moment
                ResponseType::Package(_) => {}
            }
        }
        Err(resp) => {
            resp.into_iter().for_each(|b| {
                block!(tx.write(*b)).unwrap();
            })
        }
    }
}
