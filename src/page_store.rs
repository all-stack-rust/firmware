use server::storage::Storage;
use server::http_codes::HttpCodes;

pub struct StaticStorage {}

static ROOT: &[u8] = include_bytes!("../../client/static/index.html");
static ROOT_TYPE: &str = "text/html";

static STYLE_CSS: &[u8] = include_bytes!("../../client/static/style.css");
static STYLE_CSS_TYPE: &str = "text/css";

static WASM_JS: &[u8] = include_bytes!("../../client/static/wasm.js");
static WASM_JS_TYPE: &str = "text/javascript";

static WASM_WASM: &[u8] = include_bytes!("../../client/static/wasm_bg.wasm");
static WASM_WASM_TYPE: &str = "application/wasm";

static FAVICON: &[u8] = include_bytes!("../../client/static/favicon.ico");
static FAVICON_TYPE: &str = "image/x-icon";

impl StaticStorage {
    pub fn new() -> Self {
        StaticStorage {}
    }
}

impl Storage for StaticStorage {
    fn fetch(&self, file_name: &str) -> Result<(&[u8], &str), HttpCodes> {
        match file_name {
            "/" => {
                Ok((ROOT, ROOT_TYPE))
            }
            "/index.html" => {
                Ok((ROOT, ROOT_TYPE))
            }
            "/style.css" => {
                Ok((STYLE_CSS, STYLE_CSS_TYPE))
            }
            "/wasm.js" => {
                Ok((WASM_JS, WASM_JS_TYPE))
            }
            "/wasm_bg.wasm" => {
                Ok((WASM_WASM, WASM_WASM_TYPE))
            }
//            "/favicon.ico" => {
//                Ok((FAVICON, FAVICON_TYPE))
//            }
            _ => {
                Err(HttpCodes::C404)
            }
        }
    }
}